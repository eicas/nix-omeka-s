{ pkgs ? import <nixpkgs> {} }:

with pkgs;

let
  phpFpmSocketLocation = "/run/php-fpm.sock";
  #omeka-s = ((builtins.getFlake "path:/home/aengelen/dev/omeka-s-flake").packages.x86_64-linux."omeka/omeka-s").overrideAttrs (old: {
  omeka-s = ((builtins.getFlake "git+https://codeberg.org/eicas/omeka-s-flake?rev=6ec376b47fa37ba13ecaa7ae75246e89fada6660").packages.x86_64-linux."omeka/omeka-s").overrideAttrs (old: {
    #patchFlags = [ "-p1" "-d" "lib/vendor/omeka/omeka-s" ];
    patches = [
    # avoid indexing browse and search pages
    # https://github.com/omeka/omeka-s/pull/1853
    (fetchpatch {
      url = "https://github.com/omeka/omeka-s/commit/e14ded46e137151d3f6fc20c018ba1c6af20b458.patch";
      hash = "sha256-VkSGDeSBFwGKgOTNshnvrP2XInR+0n45J2C7ejdyILA=";
    })
    # Allow specifying connection URL in the environment
    # https://github.com/omeka/omeka-s/pull/1789
    (fetchpatch {
      url = "https://github.com/omeka/omeka-s/pull/1789/commits/4aa69a7b367a094a9df3f74e4b7bca47b688ca94.patch";
      sha256 = "sha256-HFqWbob3SuirzdvRCn5DlHSEekL2TUO8L04i+y6ZrwU=";
    })
    # Allow database.ini to be missing if env var exists
    # https://github.com/omeka/omeka-s/pull/1789
    (fetchpatch {
      url = "https://github.com/omeka/omeka-s/pull/1789/commits/3e005ea245b54579a32ae6f8097d7753b1aec9ef.patch";
      sha256 = "sha256-SdfWZtqy4GhJiUkeeEMj6gLYRTMnD2NdKN0X+G1hv54=";
    })
    ];
  });
  omeka-s-modules = callPackage ./omeka-s-modules.nix {};
  nginxPort = "8080";
  nginxConf = writeText "nginx.conf" ''
    user root nobody;
    daemon off;
    error_log /dev/stdout info;
    pid /dev/null;
    events {}
    http {
      access_log /dev/stdout;
      include ${nginx}/conf/mime.types;
      server {
        listen ${nginxPort};
        index index.php;
        root /webroot;
        client_max_body_size 16M;
        location / {
          try_files $uri /index.php?$args;
        }
        location /install {
          try_files $uri /index.php?$args;
        }
        location ~ \.php {
          fastcgi_pass unix:${phpFpmSocketLocation};
          fastcgi_param SCRIPT_FILENAME /webroot$fastcgi_script_name;
          fastcgi_split_path_info ^(.+\.php)(/.+)$;
          include ${nginx}/conf/fastcgi_params;
        }
      }
    }
  '';
  phpFpmCfg = writeText "php-fpm.conf" ''
    [global]
    daemonize=yes
    error_log=/proc/self/fd/2

    [www]
    user = nobody
    group = nobody
    listen = ${phpFpmSocketLocation}
    pm = static
    pm.max_children = 5

    ; to read e.g. the database config passed in via the env.
    ; there is nothing (more) sensitive in there anyway:
    clear_env=no

    access.log=/proc/self/fd/2
    catch_workers_output = yes
    php_flag[display_errors] = on
    php_admin_value[error_log] = /proc/self/fd/2
    php_admin_flag[log_errors] = on
  '';
  phpIni = writeText "php.ini" ''
    post_max_size = 32M
    upload_max_filesize = 32M
    openssl.cafile=${cacert}/etc/ssl/certs/ca-bundle.crt
  '';
  startScript = writeScript "start.sh" ''
    #!${bash}/bin/sh
    # php 8.2 causes a problem similar to https://github.com/laminas/laminas-router/pull/40 in CleanUrl
    ${php81}/bin/php-fpm -y ${phpFpmCfg} -c ${phpIni}
    exec "${nginx}/bin/nginx" "-c" ${nginxConf}
  '';
  #omeka-s-theme-eicas = /home/aengelen/dev/Omeka-S-theme-EICAS;
  omeka-s-theme-eicas = fetchgit {
    url = https://codeberg.org/eicas/Omeka-S-theme-EICAS;
    rev = "12615fbdf87a24f2c8abb4350260df33da699ca5";
    sha256 = "sha256-LyuHno7PDiFRZVsODkZmVOnKMfSb2Lkf8PHsGrLRpJs=";
  };
  omeka-s-image = dockerTools.buildLayeredImage {
    name = "raboof/omeka-s";
    tag = "latest";
    contents = [
      dockerTools.fakeNss
      # Needed to create thumbnails
      imagemagick
      # Seems to be needed to invoke imagemagick
      bash
      # just for development/debugging, to be removed again
      #vim coreutils findutils gnugrep
    ];
    extraCommands = ''
      # nginx still tries to read this directory even if error_log
      # directive is specifying another file :/
      mkdir -p var/log/nginx
      mkdir -p var/cache/nginx
      mkdir -p run
      touch run/php-fpm.sock

      # php-fpm lock file,
      # media file upload PHP temporary directory
      # conversions temporary directory
      mkdir -p tmp
      chmod a+rwx tmp

      # the 'installer' wants to write to the app, so copy
      # it for now.
      mkdir webroot
      # TODO make omeka-s output to a more reasonable path
      cp -ra ${omeka-s}/lib/vendor/omeka/omeka-s/* webroot

      # We copy the config from the repo into the container.
      # Some modules can be configured from the web UI, and
      # write some of that config into /config. If you want
      # to use that, mount it as a volume: this way, you can
      # inspect and version-control any such changes.
      # Doing the same for 'regular' Laminas module configuration
      # options is a TODO ;)
      chmod -R a+rwx webroot/config
      rm -r webroot/config
      cp -r ${./config} webroot/config

      chmod a+rwx webroot/files

      # Can also be mounted for easier access
      chmod a+rwx webroot/logs
      chmod a+rwx webroot/logs/*

      chmod a+rwx webroot/modules
      cp -r ${omeka-s-modules.value-suggest} webroot/modules/ValueSuggest
      cp -r ${omeka-s-modules.nde-termennetwerk} webroot/modules/NdeTermennetwerk
      cp -r ${omeka-s-modules.clean-url} webroot/modules/CleanUrl
      chmod a-w webroot/modules

      chmod a+rwx webroot/themes
      cp -r ${omeka-s-theme-eicas} webroot/themes/EICAS
      chmod a-w webroot/themes

    '';
    config = {
      Cmd = [ "${startScript}" ];
      ExposedPorts = {
        "${nginxPort}/tcp" = {};
      };
    };
  };
in
  omeka-s-image 
