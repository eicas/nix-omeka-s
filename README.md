# nix-omeka-s

An attempt to build a particular [Omeka S](https://omeka.org/s/) installation using [Nix](https://nixos.org)

## Status

In use for https://collectie.eicas.nl and https://bibliotheek.eicas.nl

## Using

See `docker-compose.yml` for a standalone instance, or
https://codeberg.org/eicas/infra-collectie-bibliotheek for
a larger real-world example.

## Building

Building a docker image that will serve providence,
but will connect to an 'outside' mariadb instance for the database.

You can start it with:

```
docker load < $(nix-build .) && docker-compose up
```

and then visit https://localhost:8080

## Backups

Creating:

```
docker exec -ti <container id> mysqldump omeka -u omeka -p > dump.sql
```

Restoring:

```
docker exec -i nix-omeka-s-database-1 mysql omeka -u omeka --password=$(cat .env | grep MYSQL_PASSWORD | cut -d "=" -f 2) < dump.sql
```
