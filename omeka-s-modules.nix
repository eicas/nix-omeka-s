{ fetchFromGitHub
, fetchFromGitLab
, stdenv
, fetchpatch
, fetchgit
}:

{
  #value-suggest = /home/aengelen/dev/ValueSuggest;
  value-suggest =
    fetchFromGitHub {
      owner = "omeka-s-modules";
      repo = "ValueSuggest";
      rev = "refs/tags/v1.16.1";
      hash = "sha256-mnwVmCSp7SOlVWEUDMl+Fga93ledd+Sw/GiQnpGrWFk=";
    };
  nde-termennetwerk = stdenv.mkDerivation {
    pname = "Omeka-S-module-NdeTermennetwerk";
    version = "snapshot";
    src =
      fetchFromGitHub {
        owner = "omeka-s-modules";
        repo = "NdeTermennetwerk";
        rev = "refs/tags/v1.1.0";
        hash = "sha256-g9Nfmb1e1yG7hpbMQIi34O8tbMFfQpZYWfs4MLzzdGc=";
      };
    patches = [
      # https://github.com/omeka-s-modules/NdeTermennetwerk/pull/6
      ./omeka-s-nde-termennetwerk-rkdartists-endpoint.patch
      # Use NDE 'generic' Getty AAT until we have migrated, either
      # to using the NDE split one or the generic one
      (fetchpatch {
        url = "https://github.com/raboof/NdeTermennetwerk/commit/d3d2e467a801f6976074fe354bae7d5cdde13514.patch";
        sha256 = "sha256-5+jV5hT3rgZDi9Q/ewdRCV/8Z+IOxnu8NfDBRbNKqdo=";
      })
    ];
    installPhase = ''
      runHook preInstall
      cp -r . $out
      runHook postInstall
    '';
   };
  generic =
    fetchFromGitLab {
      owner = "Daniel-KM";
      repo = "Omeka-S-module-Generic";
      rev = "a59e2b5fc32b725a5837b4a11ba7baaba78534ba";
      hash = "sha256-XhojKehNefAH5UhQgL1RRxQ1mBK1fdj0BRRIbquhQsI=";
    };
  clean-url =
    fetchFromGitLab {
      owner = "Daniel-KM";
      repo = "Omeka-S-module-CleanUrl";
      rev = "9c833104ec5c80e5340fdb2948ca6b207f2a7c5c";
      hash = "sha256-a1CfyAkaUKDNaQfCScfDBoiqmCwMAKcDrEs0eCxyC8o=";
    };
  search =
    fetchFromGitHub {
      owner = "biblibre";
      repo = "omeka-s-module-Search";
      rev = "refs/tags/v0.11.0";
      hash = "sha256-YSVQf6WiQVCQEJvnna30EAepWmRbWbFhEklbnoKxc50=";
    };
  solr =
    fetchFromGitHub {
      owner = "biblibre";
      repo = "omeka-s-module-Solr";
      rev = "refs/tags/v0.9.4";
      hash = "sha256-VgAVvol1K0O5E4348OmXY6R/OUZ3FTP+mB3k24+2Nwk=";
    };
  #unapi =
  #  fetchFromGitLab {
  #    owner = "Daniel-KM";
  #    repo = "Omeka-S-module-UnApi";
  #    rev = "1.3.0";
  #    hash = "sha256-MlD8xKKz/b61dceKfUtvwf8xmlOAEdZYff11P3KDtdY=";
  # };
  #custom-ontology =
  #  fetchFromGitLab {
  #    owner = "Daniel-KM";
  #    repo = "Omeka-S-module-CustomOntology";
  #    rev = "3.3.5.1";
  #    hash = "sha256-+HjcCzC5sQ6Fkxt3WZungyoouM/q9t2U4R7BHF/ry+g=";
  #  };
#  oai-pmh =
#    fetchFromGitLab {
#      owner = "Daniel-KM";
#      repo = "Omeka-S-module-OaiPmhRepository";
#      rev = "3.3.5.4";
#      hash = "sha256-ln9CYcPemnvAaJj35EUhEX8md55Z9u1lFq1BsqkK+N0=";
#    };
  #oai-pmh =
  #  stdenv.mkDerivation {
  #    pname = "Omeka-S-module-OaiPmhRepository";
  #    version = "snapshot";
  #    src =
  #      fetchFromGitLab {
  #        owner = "Daniel-KM";
  #        repo = "Omeka-S-module-OaiPmhRepository";
  #        rev = "3.3.5.4";
  #        hash = "sha256-ln9CYcPemnvAaJj35EUhEX8md55Z9u1lFq1BsqkK+N0=";
  #      };
  #    patches = [
  #      (fetchpatch {
  #        url = "https://codeberg.org/eicas/Omeka-S-module-OaiPhmRepository/commit/1e1ef131411fbb108cc60fb21de019ce61198727.patch";
  #        sha256 = "sha256-/VHcWgpyJXoWqPj6trYNm0xxK2IfnAaJ4jtPjThRQAI=";
  #      })
  #      (fetchpatch {
  #        url = "https://codeberg.org/eicas/Omeka-S-module-OaiPhmRepository/commit/8800934c078f7066677057efa670f1ddc6ee773a.patch";
  #        sha256 = "sha256-BAoa6bm+8iJlGVR9OWWKdbwtCcAdi4alWKeIRWcFmUE=";
  #      })
  #      (fetchpatch {
  #        url = "https://codeberg.org/eicas/Omeka-S-module-OaiPhmRepository/commit/500b511821ec404e0f5a84be9536a3c8111d2f78.patch";
  #        sha256 = "sha256-fBuV9OyTObA3J2cGCFaJobtU2iqQT3//4NSEcTxIc1I=";
  #      })
  #    ];
  #    installPhase = ''
  #      runHook preInstall
  #      cp -r . $out
  #      runHook postInstall
  #    '';
  #  };
  #custom-vocab =
  #  fetchFromGitHub {
  #    owner = "omeka-s-modules";
  #    repo = "CustomVocab";
  #    rev = "v1.4.0";
  #    hash = "sha256-jsgU922Q728jBNsYmbK5PsuZtcR0COHJ/PDSFW5D3Ls=";
  #  };
  #item-duplicator = /home/aengelen/dev/Omeka-S-module-ItemDuplicator;
  #item-duplicator =
  #  fetchgit {
  #      url = https://codeberg.org/eicas/Omeka-S-module-ItemDuplicator;
  #      rev = "fe600c288093ce90ff34943020cd7bb987f24861";
  #      sha256 = "sha256-ZL0a/Mj9cJpQH9FxYvEm31lAj2tmQtur5QWLZ3qC4Ms=";
  #  };
}
